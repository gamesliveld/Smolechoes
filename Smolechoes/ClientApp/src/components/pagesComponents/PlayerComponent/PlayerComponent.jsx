import React, { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import './PlayerComponent.css'
import HeaderComponent from "../../structureElements/Header/HeaderComponent";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { Icon } from "leaflet";

export default function PlayerComponent() {

    const params = useParams();
    const excursionId = params.id;
    const pauseBtn = useRef(null);

    const [currentPosition, setCurrentPosition] = useState([54.782635, 32.045287]);

    const zoomLevel = 13;
    const [isActive, setIsActive] = useState(false);

    const [currentPoint, setCurrentPoint] = useState(0);

    const [excursionData, setExcursionData] = useState(null);
    useEffect(() => {
        fetch(`http://localhost:5297/api/Pathways/${excursionId}`)
            .then(res => {
                return res.json()
            })
            .then(data => {
                console.log(currentPosition);
                console.log(data);
                document.title = data.name;
                return setExcursionData(data);
            })
    }, []);

    function nextPoint() {
        if (currentPoint < excursionData.points.length - 1) {
            setCurrentPoint(currentPoint + 1);
            console.log(currentPosition);
            pauseBtn.current.classList.add('active')
        }
    }

    function playPause() {
        setIsActive(!isActive)
        pauseBtn.current.classList.toggle('active');
        let song = document.getElementById("audioPlayer");
        console.log(song.paused);
        if (song.paused)
            song.play();
        else
            song.pause();
    }

    function prevPoint() {
        if (currentPoint > 0) {
            setCurrentPoint(currentPoint - 1);
            console.log(currentPosition);
            pauseBtn.current.classList.add('active')
        }
    }

    const customIcon = new Icon({
        iconUrl: "./images/point_icon.svg",
        iconSize: [38, 38]
    });

    return (
        <div className="player-page-container">
            <audio src={excursionData !== null ? excursionData.points[currentPoint].audioUrl : null} autoplay id="audioPlayer"></audio>
            <HeaderComponent/>
            <div className="player-page-content">
                <div className="player-block">
                    <MapContainer 
                            className="player-map"
                            center={currentPosition} 
                            zoom={zoomLevel} >
                        <TileLayer
                            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                            url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                        />
                            {excursionData !== null ? excursionData.points.map(point => (
                                <Marker position={[point.latitude, point.longitude]} icon={ customIcon }>
                                    <Popup>{ point.name }</Popup>
                                </Marker>
                            )) : null}
                    </MapContainer>
                    <div className="player-controls">
                        <span className="point-name" style={excursionData ? excursionData.points[currentPoint].name.length > 17 && window.innerWidth < 480 ? {"fontSize": "18px"} : {"fontSize": "26px"} : null}>{excursionData !== null ? excursionData.points[currentPoint].name : null}</span>
                        <div className="track-line">
                            <div className="line"></div>
                            <span className="point-number">{currentPoint + 1}</span>
                        </div>
                        <div className="btns">
                            <div className="prev-point-btn" onClick={ prevPoint }>
                                <img src="./images/prevPointBtn.svg" alt="previous point button" className="prev-btn-icon"/>
                            </div>
                            <div className="pause-btn active" ref={pauseBtn} onClick={playPause}>
                                <span className="left-bar"></span>
                                <span className="right-bar_1"></span>
                                <span className="right-bar_2"></span>
                            </div>
                            <div className="next-point-btn" onClick={ nextPoint }>
                                <img src="./images/nextPointBtn.svg" alt="next point button" className="next-btn-icon"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}