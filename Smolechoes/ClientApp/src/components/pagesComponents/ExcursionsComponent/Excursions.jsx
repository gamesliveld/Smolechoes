import React, {useState, useEffect } from "react";
import HeaderComponent from "../../structureElements/Header/HeaderComponent";
import ExcursionCardComponent from "../../structureElements/ExcursionCardComponent/ExcursionCardComponent";
import './Excursions.css'


export default function Excursions() {
    document.title = "Экскурсии";

    const [excursionsData, setExcursionsData] = useState(null);
    useEffect(() => {
        fetch('http://localhost:5297/api/Pathways')
        .then(res => {
            return res.json()
        })
        .then(data => {
            return setExcursionsData(data);
        })
    }, [])
    
    return(
        <div className="excs-page-container">
            <HeaderComponent/>
            <div className="excs-page-content">
                <div className="excs-filter">Смоленск центральный</div>
                <div className="excs-cards">
                    {excursionsData ? 
                        excursionsData.map(excursion => <ExcursionCardComponent link={`/excursion/${excursion.id}`} name={excursion.name} description={excursion.description} img={excursion.imgUrl} />)
                    : null}
                </div>
            </div>
        </div>
    )
}