﻿using Microsoft.EntityFrameworkCore;

namespace Smolechoes.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Pathway> Pathways { get; set; } = null!;
        public DbSet<Point> Points { get; set; } = null!;
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Pathway>().HasData(
                new Pathway()
                {
                    Id = 1,
                    Name = "Удивительные животные города Смоленска",
                    Description = "В разных уголках города Смоленска спрятались животные: благородный олень, хищные птицы, очень умный еж. Давайте попробуем отыскать их всех!",
                    ImgUrl = "./images/amazing_animals.jpg",
                    FullDescription = "По городу Смоленску, в самых неожиданных местах, можно встретить скульптуры, посвященные братьям нашим меньшим – животным. У каждого из них есть интересная история, которой они хотят поделиться с любознательными туристами.\r\nВ парке Блонье стоит олень в полный рост, а возле культурно-выставочного центра им. Тенишевых можно встретить собаку с секретом в груди. Экскурсия расскажет об авторах памятников животным, почему они были запечатлены в металле или граните, а также о ценности животных в нашем мире.\r\n"
                },
                new Pathway()
                {
                    Id = 2,
                    Name = "Отечественная война 1812 года",
                    Description = "Экскурсия, посвящённая Отечественной войне 1812 года, с которой вы можете познакомиться поближе и узнать много нового! ",
                    ImgUrl = "./images/1812.JPG",
                    FullDescription = "Отечественная война 1812 года – это важное и ужасное событие, которое произошло в нашей стране и не обошло Смоленщину стороной. Амбиции Наполеона были разбиты Российской империй, что спасло Европу! Как сохраняется память о войне? Какие известные полководцы захоронены на Смоленской земле? Всё это вам предстоит узнать на нашем маршруте!"
                },
                new Pathway()
                {
                    Id = 3,
                    Name = "Великая Отечественная война 1941-1945 гг.",
                    Description = "Эта экскурсия посвящена самой кровопролитной и страшной войне в истории нашей страны и Смоленска в частности. ",
                    ImgUrl = "./images/1841.JPG",
                    FullDescription = "«Вставай, страна огромная, \nВставай на смертный бой\r\nС фашисткой силой тёмною, \r\nС проклятою ордой»! \r\nДанный маршрут позволит вам узнать больше о Великой Отечественной войне, а также почтить память всех граждан, которые с ней столкнулись и боролись за нашу свободу.\r\nРекомендуем вам не спешить во время прохождения экскурсии, а внимательно изучить достопримечательности для полного погружения в то тяжёлое время.\r\n"
                });

            modelBuilder.Entity<Point>().HasData(
                new Point()
                {
                    Id = 1,
                    PathwayId = 1,
                    Name = "Смоленские львы",
                    Description = null,
                    Latitude = 54.783396312825516,
                    Longitude = 32.03844261647287,
                    AudioUrl = "./audios/Удивительные_животные/1.1.wav",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 2,
                    PathwayId = 1,
                    Name = "Ученый Еж",
                    Description = null,
                    Latitude = 54.784327,
                    Longitude = 32.039653,
                    AudioUrl = "./audios/Удивительные_животные/1.2.wav",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 3,
                    PathwayId = 1,
                    Name = "Орлы-защитники",
                    Description = null,
                    Latitude = 54.779072,
                    Longitude = 32.047272,
                    AudioUrl = "./audios/Удивительные_животные/1.3.wav",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 4,
                    PathwayId = 1,
                    Name = "Памятник «Пес Полкан»",
                    Description = null,
                    Latitude = 54.783904,
                    Longitude = 32.047972,
                    AudioUrl = "./audios/Удивительные_животные/1.4.wav",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 5,
                    PathwayId = 1,
                    Name = "Бронзовый рогатый",
                    Description = null,
                    Latitude = 54.782120,
                    Longitude = 32.047674,
                    AudioUrl = "./audios/Удивительные_животные/1.5.wav",
                    ImgUrl = null
                },
                 new Point()
                 {
                     Id = 6,
                     PathwayId = 2,
                     Name = "Памятник Защитникам Смоленска", // от французских войск 1812 г.
                     Description = null,
                     Latitude = 54.783396312825516,
                     Longitude = 32.03844261647287,
                     AudioUrl = "./audios/Отечественная_война_1812/2.1.mp3",
                     ImgUrl = null
                 },
                new Point()
                {
                    Id = 7,
                    PathwayId = 2,
                    Name = "Могила генерала Гюдена",
                    Description = null,
                    Latitude = 54.784327,
                    Longitude = 32.039653,
                    AudioUrl = "./audios/Отечественная_война_1812/2.2.mp3",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 8,
                    PathwayId = 2,
                    Name = "Мемориал войны 1812 года",
                    Description = null,
                    Latitude = 54.779072,
                    Longitude = 32.047272,
                    AudioUrl = "./audios/Отечественная_война_1812/2.3.mp3",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 9,
                    PathwayId = 2,
                    Name = "Вознесенский монастырь",
                    Description = null,
                    Latitude = 54.783904,
                    Longitude = 32.047972,
                    AudioUrl = "./audios/Отечественная_война_1812/2.4.mp3",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 10,
                    PathwayId = 2,
                    Name = "Кирочная улица",
                    Description = null,
                    Latitude = 54.782120,
                    Longitude = 32.047674,
                    AudioUrl = "./audios/Отечественная_война_1812/2.5.mp3",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 11,
                    PathwayId = 3,
                    Name = "Лопатинский сад",
                    Description = null,
                    Latitude = 54.783396312825516,
                    Longitude = 32.03844261647287,
                    AudioUrl = "./audios/Великая_отечественная_война/3.1.wav",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 12,
                    PathwayId = 3,
                    Name = "Крепостная стена",
                    Description = null,
                    Latitude = 54.784327,
                    Longitude = 32.039653,
                    AudioUrl = "./audios/Великая_отечественная_война/3.2.wav",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 13,
                    PathwayId = 3,
                    Name = "Вечный огонь",
                    Description = null,
                    Latitude = 54.779072,
                    Longitude = 32.047272,
                    AudioUrl = "./audios/Великая_отечественная_война/3.3.wav",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 14,
                    PathwayId = 3,
                    Name = "Первый театр военного фронта",
                    Description = null,
                    Latitude = 54.783904,
                    Longitude = 32.047972,
                    AudioUrl = "./audios/Великая_отечественная_война/3.4.wav",
                    ImgUrl = null
                },
                new Point()
                {
                    Id = 15,
                    PathwayId = 3,
                    Name = "Олень на Блонье",
                    Description = null,
                    Latitude = 54.782120,
                    Longitude = 32.047674,
                    AudioUrl = "./audios/Великая_отечественная_война/3.5.wav",
                    ImgUrl = null
                });

        }
    }
}
